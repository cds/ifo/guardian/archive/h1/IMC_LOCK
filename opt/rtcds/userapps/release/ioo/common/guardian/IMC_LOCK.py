from guardian import GuardState, GuardStateDecorator, NodeManager
import cdsutils as cdu
import time
import math
import ISC_library
import ISC_GEN_STATES
import lscparams
nodes = NodeManager(['SUS_MC2'])

##################################################
# Function used only in IMC
##################################################
def engage_iss_third_loop():
    # for the new second loop (Sep.14.2016)
    ezca['PSL-ISS_THIRDLOOP_SERVO_TRAMP'] = 0
    ezca['PSL-ISS_THIRDLOOP_SERVO_GAIN'] = 0
    ezca.get_LIGOFilter('PSL-ISS_THIRDLOOP_SERVO').switch_on('INPUT','OUTPUT', 'DECIMATION', 'FM1', 'FM10')
    ezca['PSL-ISS_THIRDLOOP_SERVO_TRAMP'] = 10
    time.sleep(0.1)
    ezca['PSL-ISS_THIRDLOOP_SERVO_GAIN'] = 0.5

##################################################
# STATES: DOWN / OFFLINE
##################################################
request = 'LOCKED'
# We don't use third loop any more.
# nominal = 'NOMINAL_TR_CLOSED'
nominal = 'ISS_ON'

class INIT(GuardState):
    """Check lock status and jump to appropriate state.
    """
    index = 0
    request = True

    def main(self):
        nodes.set_managed()
        if not ISC_library.PSL_ready():
            return 'FAULT'
        if ISC_library.is_locked('IMC'):
            return 'ACQUIRE'
        else:
            return 'DOWN'


class FAULT(GuardState):
    """Wait for all faults to be cleared.
    """
    index = 10
    request = False
    redirect = False

    def main(self):
        # print fault notifications but don't do anything about it
        ISC_library.PSL_ready()

        # don't kick the FSS while waiting in FAULT
        ezca['IMC-REFL_SERVO_FASTEN'] = 'Off'

        # turn off MCL feedback so MC2 doesn't rail
        ezca.switch('IMC-MCL', 'OUTPUT', 'OFF')

        # disable the offsets in ASCIMC so it doesn't drift while waiting
        ezca['IMC-DOF_2_P_TRAMP'] = 0
        ezca['IMC-DOF_1_Y_TRAMP'] = 0
        ezca.switch('IMC-DOF_2_P', 'OFFSET', 'OFF')
        ezca.switch('IMC-DOF_1_Y', 'OFFSET', 'OFF')
        self.timer['wait'] = 0

    def run(self):
        if self.timer['wait']:
            if not ISC_library.PSL_ready():
                return
            self.timer['wait'] = 2
            return True


class MOVE_TO_OFFLINE(GuardState):
    """Disable the Common Mode Servo Board inputs and misalign MC2.
    """
    request = False
    redirect = False

    @nodes.checker()
    def main(self):
        if ezca['IMC-PWR_IN_OUTPUT']< 4:
            log('setting up for 1-4 Watt input')
            # 1 W configuration
            refl_servo_gain_init = -10
            refl_servo_gain_locked= 13
            trans_pd_lock_threshold = 800
            ##trans_pd_lock_threshold = 200 # 2014.Aug. somehow trans was too low
            LSC_FM_trig_thresh_on = 100
            LSC_FM_trig_thresh_off = 90

        elif ezca['IMC-PWR_IN_OUTPUT'] >= 4 and ezca['IMC-PWR_IN_OUTPUT'] < 10:
            log('setting up for 4-10 Watt input')
            # 4 W configuration
            refl_servo_gain_init = -17
            refl_servo_gain_locked= 6
            trans_pd_lock_threshold = 400
            LSC_FM_trig_thresh_on = 500
            LSC_FM_trig_thresh_off = 500

        else:
            # 10 W configuration
            log('setting up for 10 Watt input')
            refl_servo_gain_init = -23
            refl_servo_gain_locked = 0
            trans_pd_lock_threshold = 300
            LSC_FM_trig_thresh_on = 1000
            LSC_FM_trig_thresh_off = 900

        # make sure the LSC inputs on MC1 and MC3 are off
        ezca.switch('SUS-MC1_M3_LOCK_L', 'INPUT', 'OFF')
        ezca.switch('SUS-MC3_M3_LOCK_L', 'INPUT', 'OFF')

        # turn off MC2 length feedback
        ezca['SUS-MC2_M2_LOCK_L_GAIN'] = 0
        ezca.switch('SUS-MC2_M2_LOCK_L', 'FM3', 'OFF')
        ezca.switch('SUS-MC2_M1_LOCK_L', 'INPUT', 'OFF')
        ezca.get_LIGOFilter('SUS-MC2_M1_LOCK_L').ramp_gain(0, 5)
        ezca['SUS-MC2_M1_LOCK_L_RSET'] = 2
        ezca.switch('SUS-MC2_M1_LOCK_L', 'FM1', 'OFF')
        ezca['SUS-MC2_M1_LOCK_L_GAIN'] = -1

        # reset IMC "common mode" servo settings
        ezca['IMC-REFL_SERVO_IN1GAIN'] = refl_servo_gain_init
        ezca['IMC-REFL_SERVO_COMBOOST'] = 0

        # set up FM triggering
        ezca['IMC-MCL_MASK_FM10'] = 1
        ezca['IMC-MCL_FM_TRIG_THRESH_ON']  = LSC_FM_trig_thresh_on
        ezca['IMC-MCL_FM_TRIG_THRESH_OFF'] = LSC_FM_trig_thresh_off

        # VCO drive might be disabled if the FAULT state was reached
        ezca['IMC-REFL_SERVO_FASTEN'] = 'On'

        # disable the offsets in ASCIMC so it doesn't drift while waiting
        ezca['IMC-DOF_2_P_TRAMP'] = 0
        ezca['IMC-DOF_1_Y_TRAMP'] = 0
        ezca.switch('IMC-DOF_2_P', 'OFFSET', 'OFF')
        ezca.switch('IMC-DOF_1_Y', 'OFFSET', 'OFF')

        ezca['IMC-REFL_SERVO_IN1EN'] = 'Off'
        ezca['IMC-REFL_SERVO_IN2EN'] = 'Off'
        # MISALIGN MC2
        nodes['SUS_MC2'] = 'MISALIGNED'

    @nodes.checker()
    def run(self):
        # Double check that MC2 arrived at Misaligned before moving on
        if not nodes.arrived:
            return False
        return 'OFFLINE'


class OFFLINE(GuardState):
    """Common Mode Servo Board input disbaled and MC2 misaligned.

    """
    index = 80
    request = True
    redirect = False
    def main(self):
        # disable the offsets in ASCIMC.
        # WHAT ON EARTH IS THIS?  SED Jan 27 2019
        ezca['IMC-DOF_2_P_TRAMP'] = 0
        ezca['IMC-DOF_1_Y_TRAMP'] = 0
        ezca.switch('IMC-DOF_2_P', 'OFFSET', 'OFF')
        ezca.switch('IMC-DOF_1_Y', 'OFFSET', 'OFF')
    @nodes.checker()
    def run(self):
        if not (ezca['IMC-REFL_SERVO_IN1EN'] == 0 and ezca['IMC-REFL_SERVO_IN2EN'] == 0):
            return 'MOVE_TO_OFFLINE'
        if not nodes.arrived:
            return False
        for ab in ['A','B']:
            for dof in ['PIT','YAW']:
                if abs(ezca['IMC-WFS_{}_DC_{}_OUT16'.format(ab, dof)]) > 0.5:
                    notify('IMC WFS not centered')
        return True


class DOWN(GuardState):
    """Reset everything to prepare for lock acquistion.
    """
    index = 20

    # set this as a "goto" state so that guardian draws edges to this
    # state from all other states in the graph
    goto = True

    @ISC_library.check_PSL
    def main(self):
        # Turn off boosts, so ISS 1st loop can lock.
        ezca['PSL-ISS_SECONDLOOP_BOOST_1']=0
        ezca['PSL-ISS_SECONDLOOP_BOOST_2']=0
        ezca['PSL-ISS_SECONDLOOP_GAIN'] = lscparams.ISS_acquisition_gain
        # clear the ISS 2nd loop offset offload
        ezca['PSL-ISS_SECONDLOOP_REFERENCE_SERVO_RSET'] = 2
        # Turn off IMC offsets
        for ii in range(1,5):
            for py in ['P','Y']:
                ezca.switch('IMC-DOF_'+str(ii)+'_'+py, 'OFFSET', 'OFF')

        for opt in ['MC1_M3', 'MC2_M3', 'MC3_M3']:
            ezca['SUS-' + opt + '_EUL2OSEM_LOAD_MATRIX'] = 1

        # Align MC2 if its not already
        nodes['SUS_MC2'] = 'ALIGNED'

        if ezca['IMC-PWR_IN_OUTPUT']< 1:
            log('setting up for less than 1 Watt input')
            # 1 W configuration
            refl_servo_gain_init = 14
            refl_servo_gain_locked= 31
            trans_pd_lock_threshold = 400
            LSC_FM_trig_thresh_on = 1300
            LSC_FM_trig_thresh_off = 200

        elif ezca['IMC-PWR_IN_OUTPUT']>1 and ezca['IMC-PWR_IN_OUTPUT']<2:
            log('setting up for less than 2 Watt input')
            # 1 W configuration
            refl_servo_gain_init = -24 # JCD -20dB 7May2019
            refl_servo_gain_locked= -3 # JCD -20dB 7May2019
            trans_pd_lock_threshold = 400
            LSC_FM_trig_thresh_on = 1300
            LSC_FM_trig_thresh_off = 200

        elif ezca['IMC-PWR_IN_OUTPUT']>= 2 and ezca['IMC-PWR_IN_OUTPUT']< 4:
            log('setting up for 1-4 Watt input')
            # 1 W configuration
            refl_servo_gain_init = -30 # JCD -20dB 7May2019
            refl_servo_gain_locked= -7 # JCD -20dB 7May2019
            trans_pd_lock_threshold = 800
            LSC_FM_trig_thresh_on = 1300
            LSC_FM_trig_thresh_off = 200

        elif ezca['IMC-PWR_IN_OUTPUT'] >= 4 and ezca['IMC-PWR_IN_OUTPUT'] < 10:
            log('setting up for 4-10 Watt input')
            # 4 W configuration
            refl_servo_gain_init = -32 # JCD wanted -20dB, but that would be -37 on slider, so leave at -32dB 7May2019
            refl_servo_gain_locked= -14 # JCD -20dB 7May2019
            trans_pd_lock_threshold = 400
            LSC_FM_trig_thresh_on = 1300 #250 #Changed to match low power, since not really used. JCD 6Aug2019
            LSC_FM_trig_thresh_off = 200 #250 #Changed to match low power, since not really used. JCD 6Aug2019

        else:
            # 10 W configuration
            log('setting up for 10 Watt input')
            refl_servo_gain_init = -32 # JCD wanted -20dB, but that would be -43 on slider, so leave at -32dB 7May2019
            refl_servo_gain_locked = -20 # JCD -20dB 7May2019
            trans_pd_lock_threshold = 300
            LSC_FM_trig_thresh_on = 1300 #500  #Changed to match low power, since not really used. JCD 6Aug2019
            LSC_FM_trig_thresh_off = 200 #450 #Changed to match low power, since not really used. JCD 6Aug2019

        # make sure the LSC inputs on MC1 and MC3 are off
        ezca.switch('SUS-MC1_M3_LOCK_L', 'INPUT', 'OFF')
        ezca.switch('SUS-MC3_M3_LOCK_L', 'INPUT', 'OFF')

        # turn off MC2 length feedback
        ezca['SUS-MC2_M2_LOCK_L_GAIN'] = 0
        ezca.switch('SUS-MC2_M2_LOCK_L', 'FM3', 'OFF')
        ezca.switch('SUS-MC2_M1_LOCK_L', 'INPUT', 'OFF')
        ezca.get_LIGOFilter('SUS-MC2_M1_LOCK_L').ramp_gain(0, 5,wait=False)
        ezca['SUS-MC2_M1_LOCK_L_RSET'] = 2
        ezca.switch('SUS-MC2_M1_LOCK_L', 'FM1', 'OFF')
        ezca['SUS-MC2_M1_LOCK_L_GAIN'] = -1
        ezca.get_LIGOFilter('SUS-MC2_M3_LOCK_L').only_on('FM3','FM9','DECIMATION')

        # reset IMC "common mode" servo settings
        ezca['IMC-REFL_SERVO_IN1GAIN'] = refl_servo_gain_init
        ezca['IMC-REFL_SERVO_COMBOOST'] = 0
        ezca['IMC-REFL_SERVO_IN1EN'] = 'On'
        ezca['IMC-REFL_SERVO_IN2EN'] = 'Off'
        ezca['IMC-REFL_SERVO_FASTGAIN'] = 0

        # set up FM triggering
        ezca['IMC-MCL_MASK_FM10'] = 1
        ezca['IMC-MCL_FM_TRIG_THRESH_ON'] = LSC_FM_trig_thresh_on
        ezca['IMC-MCL_FM_TRIG_THRESH_OFF'] = LSC_FM_trig_thresh_off

        # VCO drive might be disabled if the FAULT state was reached
        ezca['IMC-REFL_SERVO_FASTEN'] = 'On'

        # disable the offsets in ASCIMC so it doesn't drift while waiting
        ezca['IMC-DOF_2_P_TRAMP'] = 0
        ezca['IMC-DOF_1_Y_TRAMP'] = 0
        ezca.switch('IMC-DOF_2_P', 'OFFSET', 'OFF')
        ezca.switch('IMC-DOF_1_Y', 'OFFSET', 'OFF')

        ezca['PSL-ISS_SECONDLOOP_ENABLE'] = 0
        ezca['PSL-ISS_SECONDLOOP_BOOST_1'] = 0
        ezca['PSL-ISS_SECONDLOOP_BOOST_2'] = 0
        ezca['PSL-ISS_SECONDLOOP_OUTPUT_SWITCH'] = 1
        ezca.switch('PSL-ISS_THIRDLOOP_SERVO', 'OUTPUT', 'OFF')
        ezca.switch('PSL-ISS_SECONDLOOP_AC_COUPLING_SERVO', 'INPUT','FM2','FM9','FM10', 'ON')
        ezca.switch('PSL-ISS_SECONDLOOP_AC_COUPLING_SERVO', 'FM6', 'OFF')
        ezca['PSL-ISS_SECONDLOOP_AC_COUPLING_SERVO_TRAMP'] = 10
        ezca.switch('PSL-ISS_SECONDLOOP_PD_CAL','HOLD', 'OFF')
        ezca['PSL-ISS_SECONDLOOP_AC_COUPLING_DRIVE_TRAMP'] = 0
        ezca['PSL-ISS_SECONDLOOP_AC_COUPLING_DRIVE_OFFSET'] = 0
        ezca.switch('PSL-ISS_SECONDLOOP_AC_COUPLING_DRIVE', 'INPUT', 'ON', 'HOLD', 'OFFSET', 'OFF')
        ezca['PSL-ISS_SECONDLOOP_AC_COUPLING_SERVO_GAIN'] = 1
        ezca['PSL-ISS_SECONDLOOP_AC_COUPLING_INT_OFFSET'] = 0
        ezca['PSL-ISS_SECONDLOOP_AC_COUPLING_INT_OFSEN'] = 0
        ezca.switch('PSL-ISS_SECONDLOOP_REFERENCE_SERVO', 'INPUT', 'ON')
        ezca.switch('PSL-ISS_SECONDLOOP_REFERENCE_SERVO', 'OFFSET', 'OFF')
        ezca['PSL-ISS_SECONDLOOP_REFERENCE_SERVO_OFFSET'] = 0
        #this is to make the ISS second loop diffracted power servo OK at 2W input power. alog 46229 and 46317
        ezca['PSL-ISS_SECONDLOOP_REFERENCE_IN_MTRX_1_4'] = 0
##################################################
# STATES: ACQUIRE through LOCKED
##################################################

class ACQUIRE(GuardState):
    """Align MC2, turn on feedback, and wait for lock.
    """
    index = 30
    request = False

    @ISC_library.check_PSL
    def main(self):
        # Align MC2 if its not already
        nodes['SUS_MC2'] = 'ALIGNED'

        # turn on MC board IN1 engage
        ezca['IMC-REFL_SERVO_IN1EN'] = 1

        # turn on MC2 length feedback
        ezca.switch('IMC-MCL', 'OUTPUT', 'ON') # in case FAULT was reached, turn on MCL
        ezca['IMC-MCL_GAIN'] = lscparams.IMC_MCL_gain
        ezca.switch('SUS-MC2_M3_LOCK_L', 'INPUT', 'OUTPUT', 'ON')
        ezca.switch('SUS-MC2_M2_LOCK_L', 'INPUT', 'OUTPUT', 'ON')
        ezca.switch('SUS-MC2_M1_LOCK_L', 'INPUT', 'OUTPUT', 'ON')

        # Jnote: can we delete these, if we trust SDF?
        # turn on pitch/yaw feedback to all MC sus
        for py in ['P','Y']:
            for Mnum in [1,2,3]:
                for sus in ['MC1','MC2','MC3']:
                    ezca.switch('SUS-%s_M%s_LOCK_%s'%(sus,Mnum,py), 'INPUT', 'OUTPUT', 'ON')

        # set up buttkick ()
        self.buttkick_tramp = ezca['SUS-MC2_M1_DRIVEALIGN_L2L_TRAMP'] + 2

        self.timer['ASC_check'] = 120.
        self.timer['IMC_buttkick'] = self.buttkick_tramp
        self.MC2_L2L_FM = ezca.get_LIGOFilter('SUS-MC2_M1_DRIVEALIGN_L2L')

    @ISC_library.check_PSL
    def run(self):
        if ISC_library.is_locked('IMC'):
            return True
        else:
            ###   Open IMC REFL PD shutter
            # CRC:  This sometimes doesn't work if the Trigger volts are too high and coming back down
            #       Moving to the run() function.
            #       ezca['SYS-MOTION_C_SHUTTER_A_STATE'] == 1 when closed

            if ezca['SYS-MOTION_C_SHUTTER_A_STATE']:
                ezca['SYS-MOTION_C_SHUTTER_A_OPEN'] = 1

            log('waiting for lock...')
            # IMC buttkick in case it stalls in the locking process
            if self.timer['IMC_buttkick']:
                self.MC2_L2L_FM.switch('OFFSET', 'PRESS')
                self.timer['IMC_buttkick'] = self.buttkick_tramp

            # if ASC outputs are large, notify the user that there is a problem
            # and suggesting clear filters' history
            if self.timer['ASC_check']:
                self.timer['ASC_check'] = 120.
                DOF_name_list=['1_P', '1_Y', '2_P', '2_Y', '3_P', '3_Y', '4_P', '4_Y']
                LowerBound = -200.
                UpperBound = 200.
                for z in range(8):
                    output = ezca['IMC-DOF_'+DOF_name_list[z]+'_OUTPUT']
                    if ((output > UpperBound) or (output < LowerBound)):
                        notify('May need to clear the history of IMC-DOF_'+DOF_name_list[z]+'!')


class BOOST(GuardState):
    """Turn on boost filters and gains.
    """
    index = 40
    request = False

    @ISC_library.assert_dof_locked_gen(['IMC'])
    def main(self):
        # turn on MC2 boost filters
        ezca['SUS-MC2_M2_LOCK_L_GAIN'] = 0.1
        ezca.switch('SUS-MC2_M2_LOCK_L', 'FM3', 'ON')

        # M1 gain is now flipped due to the sign flip in the
        # coil drivers of the suspension
        ezca['SUS-MC2_M1_LOCK_L_GAIN'] = -1.0
        ezca.switch('SUS-MC2_M1_LOCK_L', 'FM1', 'ON')

        ezca['IMC-REFL_SERVO_COMBOOST'] = 1

        self.timer['boost_wait'] = 2
        self.boosted = False

    @ISC_library.IMC_power_adjust
    @ISC_library.assert_dof_locked_gen(['IMC'])
    def run(self):
        # run the fault check function, which prints fault
        # notificaitons, but don't do anything since we don't want to
        # break lock unnecessarily.
        ISC_library.PSL_ready()

        if self.timer['boost_wait']:
            #ISC_library.IMC_power_adjust_func()
            if not self.boosted:
                ezca['IMC-REFL_SERVO_IN1GAIN'] += 1
                time.sleep(0.1)
                if ezca['IMC-REFL_SERVO_IN1GAIN'] >= lscparams.IMC_IN1_gain: # CC 13 dB, 2019 Jan 23, should be 25 dB as of Oct 08, 2018, CRC
                    self.boosted = True
            else:
                return True


class LOCKED(GuardState):
    """Final locked state.
    """
    index = 100

    @ISC_library.assert_dof_locked_gen(['IMC'])
    def main(self):
        # enable the magical offsets in imcasc to minimized the jitter-intensity coupling.
        ezca['IMC-DOF_2_P_TRAMP'] = 3
        ezca['IMC-DOF_1_Y_TRAMP'] = 3
        #ezca.switch('IMC-DOF_2_P', 'OFFSET', 'ON')
        #ezca.switch('IMC-DOF_1_Y', 'OFFSET', 'ON')
        # clear the ISS 2nd loop offset offload
        ezca['PSL-ISS_SECONDLOOP_REFERENCE_SERVO_RSET'] = 2
        
        # Turn off the 'buttkick' offset, if it got left on
        ezca.get_LIGOFilter('SUS-MC2_M1_DRIVEALIGN_L2L').switch_off('OFFSET')

    @ISC_library.assert_dof_locked_gen(['IMC'])
    @ISC_library.IMC_power_adjust
    def run(self):
        # run the fault check function, which prints fault
        # notificaitons, but don't do anything since we don't want to
        # break lock unnecessarily.
        ISC_library.PSL_ready()

        return True


##################################################
# STATES: ISS
##################################################

class CLOSE_ISS(GuardState):
    index = 50
    request = False

    @ISC_library.assert_dof_locked_gen(['IMC'])
    @ISC_library.IMC_power_adjust
    def main(self):
        self.second_loop_on = False
        self.counter=0
        self.diff_before_secloop = ezca['PSL-ISS_DIFFRACTION_AVG']
        if ISC_library.iss_ok():
            self.timer['first_loop_pause'] = 0
        else:
            notify('first loop open!')
            self.timer['first_loop_pause'] = 75 #15 JCD, DS, 30Apr2021

    @ISC_library.assert_dof_locked_gen(['IMC'])
    @ISC_library.IMC_power_adjust   
    def run(self):
        # run the fault check function, which prints fault
        # notificaitons, but don't do anything since we don't want to
        # break lock unnecessarily.
        ISC_library.PSL_ready()

        if ISC_library.iss_ok():
            if self.counter == 0: 
                ezca['PSL-ISS_SECONDLOOP_OUTPUT_SWITCH'] = 1
                self.counter += 1
            if self.counter == 1 and self.timer['first_loop_pause']:
                ezca['PSL-ISS_SECONDLOOP_AC_COUPLING_SERVO_GAIN'] = 0.0
                self.timer['hold_coupling_drive'] = ezca['PSL-ISS_SECONDLOOP_AC_COUPLING_SERVO_TRAMP']
                self.counter += 1
            if self.counter == 2 and self.timer['hold_coupling_drive']:
                ezca['PSL-ISS_SECONDLOOP_AC_COUPLING_DRIVE_TRAMP'] = 0
                ac_drive_avg = cdu.avg(-10, 'PSL-ISS_SECONDLOOP_AC_COUPLING_DRIVE_OUTPUT')
                ezca.switch('PSL-ISS_SECONDLOOP_AC_COUPLING_DRIVE', 'HOLD', 'ON')
                ezca['PSL-ISS_SECONDLOOP_AC_COUPLING_DRIVE_OFFSET'] = ac_drive_avg
                ezca.switch('PSL-ISS_SECONDLOOP_AC_COUPLING_DRIVE', 'INPUT', 'OFF')
                ezca.switch('PSL-ISS_SECONDLOOP_AC_COUPLING_DRIVE', 'OFFSET', 'ON')
                ezca.switch('PSL-ISS_SECONDLOOP_AC_COUPLING_DRIVE', 'HOLD', 'OFF')
                ezca.switch('PSL-ISS_SECONDLOOP_AC_COUPLING_DRIVE', 'HOLD', 'ON')
                time.sleep(1)
                if abs(ezca['PSL-ISS_SECONDLOOP_AC_COUPLING_DRIVE_OUTPUT'] - ac_drive_avg) < 100:
                    ezca.switch('PSL-ISS_SECONDLOOP_AC_COUPLING_DRIVE', 'OFFSET', 'OFF')
                    ezca.switch('PSL-ISS_SECONDLOOP_AC_COUPLING_DRIVE', 'INPUT', 'ON')
                    ezca['PSL-ISS_SECONDLOOP_AC_COUPLING_DRIVE_OFFSET'] = 0
                    self.timer['hold_coupling_drive'] = 1
                    self.counter += 1
                else:
                    notify('Second loop AC coupling out of range, waiting 1 second to try again!')
                    self.timer['ac pause'] = 1
            if self.counter == 3 and self.timer['hold_coupling_drive']:
                ezca['PSL-ISS_SECONDLOOP_ENABLE'] = 1
                time.sleep(0.3)
                ezca['PSL-ISS_SECONDLOOP_BOOST_1'] = 1
                ezca['PSL-ISS_SECONDLOOP_BOOST_2'] = 1
                self.timer['secondloop_check'] = 30
                self.timer['hold_coupling_drive'] = 1 # re-initializing, so can re-use this timer for increasing second loop gain
                self.counter += 1
                
            if self.counter == 4:
                if self.timer['hold_coupling_drive']:
                    if ezca['PSL-ISS_SECONDLOOP_GAIN'] < lscparams.ISS_FinalGain:
                        ezca['PSL-ISS_SECONDLOOP_GAIN'] += 1
                        self.timer['hold_coupling_drive'] = 1
                else:
                    self.counter += 1
                
            if self.counter == 5:
                if self.timer['secondloop_check']:
                    # Check if the diffracted power jumped when secondloop turned on - RWS 4May2023
                    if abs(self.diff_before_secloop - ezca['PSL-ISS_DIFFRACTION_AVG']) > (self.diff_before_secloop * 0.15): 
                        notify('Diffracted power jumped too much, toggling secondloop')
                        return 'OPEN_ISS'
                    return True
        else:
            notify('first loop open!')
            # Reset second loop's AC coupling, set counter to zero to try again
            self.counter = 0
            ezca['PSL-ISS_SECONDLOOP_AC_COUPLING_SERVO_GAIN'] = 1
            ezca.switch('PSL-ISS_SECONDLOOP_AC_COUPLING_DRIVE', 'HOLD', 'OFF')
            self.timer['first_loop_pause'] = 75 # Incr from 15
        


class ISS_ON(GuardState):
    """ISS second loop closed.
    """
    index = 70

    @ISC_library.assert_dof_locked_gen(['IMC'])
    #@ISC_library.IMC_power_adjust
    @ISC_library.iss_checker 
    def main(self):
        log('ISS Second Loop Turned On')

    @ISC_library.assert_dof_locked_gen(['IMC'])
    #@ISC_library.IMC_power_adjust  #SED JW commenting this out, we can't adjsut power while the ISS is DC coupled anyway alog 79998
    @ISC_library.iss_checker 
    def run(self):
        # run the fault check function, which prints fault
        # notificaitons, but don't do anything since we don't want to
        # break lock unnecessarily.
        ISC_library.PSL_ready()
      
        return True

class OPEN_ISS(GuardState):
    """Turn off the ISS second loop.
    """
    index = 60
    request = False


    @ISC_library.assert_dof_locked_gen(['IMC'])
    def main(self):
        ezca['PSL-ISS_SECONDLOOP_OUTPUT_SWITCH'] = 0
        time.sleep(0.1)
        ezca['PSL-ISS_SECONDLOOP_ENABLE'] = 0
        ezca['PSL-ISS_SECONDLOOP_BOOST_1'] = 0
        ezca['PSL-ISS_SECONDLOOP_BOOST_2'] = 0
        ezca.switch('PSL-ISS_SECONDLOOP_AC_COUPLING_DRIVE', 'HOLD', 'OFF')
        ezca['PSL-ISS_SECONDLOOP_AC_COUPLING_SERVO_GAIN'] = 1
        # clear the ISS 2nd loop offset offload
        ezca['PSL-ISS_SECONDLOOP_REFERENCE_SERVO_RSET'] = 2
        self.timer['wait']=20 # increased from 1 - RWS 10Jun2023
        self.counter = 0

    @ISC_library.assert_dof_locked_gen(['IMC'])
    def run(self):
        # run the fault check function, which prints fault
        # notificaitons, but don't do anything since we don't want to
        # break lock unnecessarily.
        ISC_library.PSL_ready()

        if self.timer['wait'] and self.counter == 0:
            # Reset second loop gain to acquisition gain
            ezca['PSL-ISS_SECONDLOOP_GAIN'] = lscparams.ISS_acquisition_gain
            self.timer['wait'] = 1
            self.counter += 1

        if self.timer['wait'] and self.counter == 1:
            ezca['PSL-ISS_SECONDLOOP_OUTPUT_SWITCH'] = 1 # turning the output switch on
            return True

class OFFLOAD_MCWFS(GuardState):
    '''
    Offload the IMC WFS, requires special attention because of the MC PZT. (alog 42183)
    '''
    request = False # can't request this guardian state
    redirect = False # need to finish this offload before doing other stuff


    #@ISC_library.assert_dof_locked_gen(['IMC'])
    def main(self):
        self.ramptime = 10
        self.opticlist = ['MC1', 'MC2', 'MC3']
        log('starting smooth offload for IMC WFS')
        log(self.opticlist)

        # Gather filter modules
        self.dof_fm = {}
        self.pzt_fm = {}
        self.lock_fm = {}
        self.drivealign_fm = {}
        self.opticalign_fm = {}

        for angleName in ['PIT', 'YAW']:
            for number in range(1,4): # dofs 1,2,3
                dof_fm_name = 'IMC-DOF_{0}_{1}'.format(number, angleName[0])
                self.dof_fm[dof_fm_name] = ezca.get_LIGOFilter(dof_fm_name)
            for optic in self.opticlist:
                lock_fm_name = 'SUS-{0}_M1_LOCK_{1}'.format(optic, angleName[0])
                self.lock_fm[lock_fm_name] = ezca.get_LIGOFilter(lock_fm_name)

                drivealign_fm_name = 'SUS-{0}_M1_DRIVEALIGN_{1}2{1}'.format(optic, angleName[0])
                self.drivealign_fm[drivealign_fm_name] = ezca.get_LIGOFilter(drivealign_fm_name)

                opticalign_fm_name = 'SUS-{0}_M1_OPTICALIGN_{1}'.format(optic, angleName[0])
                self.opticalign_fm[opticalign_fm_name] = ezca.get_LIGOFilter(opticalign_fm_name)

            pzt_fm_name = 'IMC-PZT_{0}'.format(angleName)
            self.pzt_fm[pzt_fm_name] = ezca.get_LIGOFilter(pzt_fm_name)

        ## Record initial state and calculate offset changes required ##
        # Record IMC M1 Lock gains and tramps
        self.lock_gain = {}
        self.lock_tramp = {}
        for lock_fm_name in self.lock_fm.keys():
            self.lock_gain[lock_fm_name] = self.lock_fm[lock_fm_name].GAIN.get()
            self.lock_tramp[lock_fm_name] = self.lock_fm[lock_fm_name].TRAMP.get()

        # Record OPTICALIGN gains, tramps, and offsets
        self.opticalign_gain = {}
        self.opticalign_tramp = {}
        self.opticalign_offset = {}
        for opticalign_fm_name in self.opticalign_fm.keys():
            self.opticalign_gain[opticalign_fm_name] = self.opticalign_fm[opticalign_fm_name].GAIN.get()
            self.opticalign_tramp[opticalign_fm_name] = self.opticalign_fm[opticalign_fm_name].TRAMP.get()
            self.opticalign_offset[opticalign_fm_name] = self.opticalign_fm[opticalign_fm_name].OFFSET.get()

        # Record DRIVEALIGN output
        self.drivealign_output = {}
        for drivealign_fm_name in self.drivealign_fm.keys():
            self.drivealign_output[drivealign_fm_name] = self.drivealign_fm[drivealign_fm_name].OUTPUT.get()

        # Record PZT offsets and input
        self.pzt_offset = {}
        self.pzt_input = {}
        self.pzt_tramp = {}
        for pzt_fm_name in self.pzt_fm.keys():
            log(pzt_fm_name)
            self.pzt_offset[pzt_fm_name] = self.pzt_fm[pzt_fm_name].OFFSET.get()
            self.pzt_input[pzt_fm_name] = ezca[pzt_fm_name+'_INMON']
            self.pzt_tramp[pzt_fm_name] = self.pzt_fm[pzt_fm_name].TRAMP.get()
            log('Initial Offset {0} = {1}'.format(pzt_fm_name, self.pzt_offset[pzt_fm_name]))
            log('Initial Input  {0} = {1}'.format(pzt_fm_name, self.pzt_input[pzt_fm_name]))

        ## Open IMC WFS loops for offloading ##
        # Freeze IMC WFS loop by shutting down IMC DOF inputs
        log('Shut down IMC DOF inputs')
        for dof_fm_name in self.dof_fm.keys():
            ezca.get_LIGOFilter(dof_fm_name).switch_off('INPUT')

        # Ramp Lock gain to zero
        log('Ramp Lock gains to zero')
        for lock_fm_name in self.lock_fm.keys():
            ezca.get_LIGOFilter(lock_fm_name).ramp_gain(0, self.ramptime, wait=False)

        # Hold outputs on the PZTs
        log('Holding PZT outputs')
        for pzt_fm_name in self.pzt_fm.keys():
            self.pzt_fm[pzt_fm_name].switch_on('HOLD')

        self.counter = 0
        self.timer['tramp'] = self.ramptime

    #@ISC_library.assert_dof_locked_gen(['IMC'])
    def run(self):

        if self.counter == 0 and self.timer['tramp']:
            ## Calculate new offsets for both PZTs and OPTICALIGNs, and apply them ##
            log('Writing new offsets')
            for angleName in ['PIT', 'YAW']:
                for optic in self.opticlist:
                    lock_fm_name = 'SUS-{0}_M1_LOCK_{1}'.format(optic, angleName[0])
                    drivealign_fm_name = 'SUS-{0}_M1_DRIVEALIGN_{1}2{1}'.format(optic, angleName[0])
                    opticalign_fm_name = 'SUS-{0}_M1_OPTICALIGN_{1}'.format(optic, angleName[0])

                    curOffset = self.opticalign_offset[opticalign_fm_name] + round(self.drivealign_output[drivealign_fm_name]/self.opticalign_gain[opticalign_fm_name], 4)
                    ezca.get_LIGOFilter(opticalign_fm_name).ramp_offset(curOffset, self.ramptime, wait=False)

                pzt_fm_name = 'IMC-PZT_{0}'.format(angleName)
                curOffset = self.pzt_offset[pzt_fm_name] + self.pzt_input[pzt_fm_name]
                ezca.get_LIGOFilter(pzt_fm_name).ramp_offset(curOffset, self.ramptime, wait=False)

            log('Waiting for ramps to finish...')                
            self.timer['tramp'] = self.ramptime
            self.counter += 1


        if self.counter == 1 and self.timer['tramp']:
            log('Done waiting')

            ## Restore initial state ##
            # Clear PZT, LOCK, and DOF filter history (integrators are in the DOF filter modules, PZT input must be cleared as well)
            log('Restore initial state')
            log('Resetting PZT, Lock, and DOF filter modules to clear filter history')
            for pzt_fm_name in self.pzt_fm.keys():
                ezca['%s_RSET'%pzt_fm_name] = 2
            for lock_fm_name in self.lock_fm.keys():
                ezca['%s_RSET'%lock_fm_name] = 2
            for dof_fm_name in self.dof_fm.keys():
                ezca['%s_RSET'%dof_fm_name] = 2

            # Unhold outputs on the PZTs
            log('Unholding PZT outputs')
            for pzt_fm_name in self.pzt_fm.keys():
                self.pzt_fm[pzt_fm_name].switch_off('HOLD')

            # Close IMC WFS loops again
            log('Turn on IMC DOF inputs')
            for dof_fm_name in self.dof_fm.keys():
                ezca.get_LIGOFilter(dof_fm_name).switch_on('INPUT')

            # Ramp Lock gain back up
            log('Ramp Lock gains back to original values')
            for lock_fm_name in self.lock_fm.keys():
                ezca.get_LIGOFilter(lock_fm_name).ramp_gain(self.lock_gain[lock_fm_name], self.lock_tramp[lock_fm_name], wait=False)

            # Rewrite tramps
            log('Restore opticalign, lock, pzt tramp times')
            for opticalign_fm_name in self.opticalign_fm.keys():
                ezca['%s_TRAMP'%opticalign_fm_name] = self.opticalign_tramp[opticalign_fm_name]
            for lock_fm_name in self.lock_fm.keys():
                ezca['%s_TRAMP'%lock_fm_name] = self.lock_tramp[lock_fm_name]
            for pzt_fm_name in self.pzt_fm.keys():
                ezca['%s_TRAMP'%pzt_fm_name] = self.pzt_tramp[pzt_fm_name]


            log('Waiting for ramps to finish...')                
            self.timer['tramp'] = self.ramptime
            self.counter += 1


        if self.counter == 2 and self.timer['tramp']:
            log('Done waiting')
            log('IMC WFS Offloaded')
            return True


class MCWFS_OFFLOADED(GuardState):
    '''
    IMC WFS offloading completed state
    '''
    request = True # can request this guardian state
    redirect = True # don't need to finish, offload has already happened

    @ISC_library.assert_dof_locked_gen(['IMC'])
    def run(self):
        return True

#OFFLOAD_MCWFS   = ISC_GEN_STATES.gen_OFFLOAD_ALIGNMENT_MANY('IMC', 10, ['MC1', 'MC2', 'MC3'])

##################################################
# EDGES
##################################################

edges = [
    ('FAULT',               'DOWN'),
    ('INIT',                'DOWN'),
    ('DOWN',                 'DOWN'),#adding an edge from DOWN to DOWN, this way when we are in DOWN and ISC_LOCK requests down, the main will be run again.
    ('DOWN',                'ACQUIRE'),
    ('ACQUIRE',             'BOOST'),
    ('BOOST',               'LOCKED'),
    ('LOCKED',              'CLOSE_ISS'),
    ('LOCKED',              'OFFLOAD_MCWFS'),
    ('OFFLOAD_MCWFS',       'MCWFS_OFFLOADED'),
    ('MCWFS_OFFLOADED',     'LOCKED'),
    ('CLOSE_ISS',           'ISS_ON'),
    ('ISS_ON',              'OPEN_ISS'),
    ('OPEN_ISS',            'LOCKED'),
    ('DOWN',                'MOVE_TO_OFFLINE'),
    ('MOVE_TO_OFFLINE',     'OFFLINE'),
    ('OFFLINE',             'DOWN'),
    ]
